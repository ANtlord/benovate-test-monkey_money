from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser
from django.core.validators import int_list_validator
from django.core.validators import MinLengthValidator


TAX_ID_LEN = 10
TAX_ID_VALIDATORS = [int_list_validator(sep=''), MinLengthValidator(TAX_ID_LEN)]


class User(AbstractUser):
    balance = models.DecimalField(max_digits=10, decimal_places=2,
                                  null=False, blank=False, default=0)
    tax_id = models.CharField(max_length=TAX_ID_LEN, null=False, blank=False,
                              validators=TAX_ID_VALIDATORS)

    def __str__(self):
        return '%s' % self.username

    def admin_email(self):
        return self.email if self.email else 'no email'
