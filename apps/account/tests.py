from django.test import TestCase
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError

User = get_user_model()

class TestUserClean(TestCase):
    def test_tax_id(self):
        """Testing empty tax number."""
        with self.assertRaises(ValidationError) as error:
            User(username='Sean', password='password').full_clean()

        with self.assertRaises(ValidationError) as error:
            User(username='Sean', password='password', tax_id='123').full_clean()

        User(username='Sean', password='password', tax_id='1234567890').full_clean()
