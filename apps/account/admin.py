from django.contrib import admin
from .models import User

from django.contrib.auth.admin import UserAdmin as Base


class UserAdmin(Base):
    list_display = 'username', 'admin_email', 'balance', 'tax_id', 'is_active'
    search_fields = 'username', 'email'
    list_editable = 'balance',

    Base.fieldsets[0][1]['fields'] += ('balance', 'tax_id')


admin.site.register(User, UserAdmin)
