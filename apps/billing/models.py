from decimal import Decimal as dec

from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator
from django.core.exceptions import ValidationError

User = get_user_model()


def validate_user_balance(amount: dec, from_user: User):
    balance = from_user.balance
    name = from_user.username
    if amount > balance:
        raise ValidationError('Not enough money. The user %s has: %s' % (name, balance))


class Transaction(models.Model):
    from_user = models.ForeignKey(User, null=False)
    amount = models.DecimalField(max_digits=10, decimal_places=2, null=False,
                                 validators=[MinValueValidator(dec('0.01'))], default=0)

    def clean_amount(self):
        try:
            from_user = self.from_user
            validate_user_balance(self.amount, self.from_user)
        except AttributeError:
            pass

    def clean(self):
        self.clean_amount()

    def _serialize_receivers(self) -> str:
        return ', '.join((str(x) for x in self.transactionreceiver_set.all()))

    def admin_receivers(self) -> str:
        return self._serialize_receivers()

    def __str__(self):
        return '%s (%s -> %s)' % (self.amount, self.from_user.tax_id, self._serialize_receivers())


class TransactionReceiver(models.Model):
    user = models.ForeignKey(User, null=False)
    transaction = models.ForeignKey(Transaction, null=False)

    def __str__(self):
        return self.user.tax_id
