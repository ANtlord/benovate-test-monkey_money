from typing import List
from decimal import Decimal as dec

from django import forms
from django.forms import inlineformset_factory
from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import ValidationError

from apps.account.models import TAX_ID_VALIDATORS

from .models import Transaction
from .models import TransactionReceiver
from .models import validate_user_balance

User = get_user_model()

class TransactionForm(forms.ModelForm):

    receivers_tax_id = forms.CharField()

    class Meta:
        model = Transaction
        fields = 'from_user', 'receivers_tax_id', 'amount',

    def clean_receivers_tax_id(self):
        tax_id = self.cleaned_data['receivers_tax_id']
        receivers_tax_id = tax_id.split(',')
        receivers_tax_id = [x.strip() for x in receivers_tax_id]
        receivers = User.objects.filter(tax_id__in=receivers_tax_id)

        taxes = {x.tax_id for x in receivers}
        for tax_id in receivers_tax_id:
            if tax_id not in taxes:
                raise ValidationError('User with tax number %s does\'t exists' % tax_id)

        return receivers

    def clean_amount(self):
        from_user = self.cleaned_data.get('from_user', None)
        if not from_user:
            raise ValidationError('It\'s impossible to get available amount of the user. '
                                  'The user doesn\'t exist.')

        amount = self.cleaned_data['amount']

        if amount.as_tuple().exponent < -2:
            raise ValidationError('Precision of value must be less than 3 digits after point')

        validate_user_balance(amount, from_user)

        receivers = self.cleaned_data.get('receivers_tax_id', None)
        if receivers:
            amount_part = amount / len(receivers)
            if (amount / len(receivers)).as_tuple().exponent < -2:
                raise ValidationError('Amount can\'t be divided in number of receivers equal')
        return amount

    def save(self, commit=True):
        from_user = self.cleaned_data['from_user']  # type: User
        self.instance.from_user = from_user

        receivers = self.cleaned_data['receivers_tax_id']

        transactions = []
        for receiver in receivers:
            transactions.append(TransactionReceiver(user=receiver, transaction=self.instance))

        res = super(TransactionForm, self).save(commit)

        for item in transactions:
            item.transaction_id = item.transaction.id
            item.save()

        amount = self.instance.amount
        cmd = ReceiverBalanceCommand(receivers, amount)
        cmd.make()

        try:
            from_user.balance -= amount
            from_user.save()
        except Exception as e:
            cmd.rollback()
            raise e

        return res


class ReceiverBalanceCommand:
    def __init__(self, receivers: List[User], amount: dec):
        self.receivers = receivers
        self.amount = amount

        receivers_count = dec('%i' % len(self.receivers))
        self.receiver_amount = amount / receivers_count

        self.saved_receivers = []

    def make(self):
        for item in self.receivers:
            item.balance += self.receiver_amount

        for item in self.receivers:
            item.save()
            self.saved_receivers.append(item)

    def rollback(self):
        for item in self.saved_receivers:
            item.balance -= self.receiver_amount

        for item in self.saved_receivers:
            item.save()
