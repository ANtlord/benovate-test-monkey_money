from decimal import Decimal as dec
from unittest.case import TestCase

from django.core.exceptions import ValidationError

from apps.billing.forms import TransactionForm
from apps.billing.models import TransactionReceiver
from apps.billing.models import Transaction

from .shortcuts import User
from .shortcuts import _setup_users
from .shortcuts import STEVE_TAX_ID
from .shortcuts import STELLA_TAX_ID
from .shortcuts import SEAN_TAX_ID


def uid(username):
    return User.objects.get(username=username).id


class CleanTransactionFormTest(TestCase):
    def setUp(self):
        _setup_users()

    def test_receivers_tax_id(self):
        """Testing validation of tax number"""
        # TODO: make case when user sends money to himself.
        cases = ('123, 123', 'any string', '111', '3333333333')
        for case in cases:
            data = {
                'receivers_tax_id': case,
                'from_user': uid('Steve'),
                'amount': dec('20'),
            }
            form = TransactionForm(data=data)
            self.assertFalse(form.is_valid())

        data = {
            'receivers_tax_id': STELLA_TAX_ID,
            'from_user': uid('Steve'),
            'amount': dec('20'),
        }
        form = TransactionForm(data=data)
        self.assertTrue(form.is_valid())

    def test_from_username(self):
        data = {
            'receivers_tax_id': STEVE_TAX_ID,
            'from_user': 123,
            'amount': dec('20'),
        }
        form = TransactionForm(data=data)
        self.assertFalse(form.is_valid())

        data = {
            'receivers_tax_id': STEVE_TAX_ID,
            'from_user': uid('Stella'),
            'amount': dec('20'),
        }
        form = TransactionForm(data=data)
        self.assertTrue(form.is_valid())

    def test_amount(self):
        cases = ('101', '0', '-101')
        for case in cases:
            data = {
                'receivers_tax_id': STEVE_TAX_ID,
                'from_user': uid('Stella'),
                'amount': dec(case),
            }
            form = TransactionForm(data=data)
            self.assertFalse(form.is_valid())

        data = {
            'receivers_tax_id': STEVE_TAX_ID,
            'from_user': uid('Stella'),
            'amount': dec('10'),
        }
        form = TransactionForm(data=data)
        self.assertTrue(form.is_valid())

    def test_amount_receivers_count(self):
        """Testing case when transaction amount cannot be divided in number of receivers"""
        ODD_AMOUNT = '0.55'
        data = {
            'receivers_tax_id': '%s, %s' % (STELLA_TAX_ID, SEAN_TAX_ID),
            'from_user': uid('Steve'),
            'amount': dec(ODD_AMOUNT),
        }
        form = TransactionForm(data=data)
        self.assertFalse(form.is_valid())

    def tearDown(self):
        User.objects.all().delete()


class TestFromUserBalanceAffects(TestCase):

    def setUp(self):
        _setup_users()

    def _make_transaction(self, tax_numbers: str, from_username, amount):
        data = {
            'receivers_tax_id': tax_numbers,
            'from_user': uid(from_username),
            'amount': amount,
        }

        form = TransactionForm(data=data)
        self.assertTrue(form.is_valid())
        form.save()

    def test_one_to_one_user(self):
        """Testing transaction from one user to unique tax number."""
        stella_balance = User.objects.get(username='Stella').balance
        sean_balance = User.objects.get(username='Sean').balance
        amount = dec('10')

        self._make_transaction(SEAN_TAX_ID, 'Stella', amount)

        stella = User.objects.get(username='Stella')
        sean = User.objects.get(username='Sean')
        self.assertEqual(stella.balance + amount, stella_balance)
        self.assertEqual(sean.balance - amount, sean_balance)

    def test_one_to_one_tax(self):
        """Tesing transaction from one user to NOT unique tax number."""
        uget = User.objects.get
        usernames = ('Stella', 'Steve', 'Stevie')
        balances = {x: uget(username=x).balance for x in usernames}

        user_with_same_tax_count = 2
        amount = dec('10')
        amount_per_receiver = amount / dec('%i' % user_with_same_tax_count)
        self._make_transaction(STEVE_TAX_ID, 'Stella', amount)
        users = {x: uget(username=x) for x in usernames}

        self.assertEqual(users['Steve'].tax_id, users['Stevie'].tax_id)
        self.assertEqual(users['Stella'].balance + amount, balances['Stella'])
        self.assertEqual(users['Steve'].balance - amount_per_receiver, balances['Steve'])
        self.assertEqual(users['Stevie'].balance - amount_per_receiver, balances['Stevie'])

    def test_one_to_many_users(self):
        """Tesing transaction from one user to several users with unique tax number."""
        uget = User.objects.get
        usernames = ('Steve', 'Stella', 'Sean')
        balances = {x: uget(username=x).balance for x in usernames}

        user_with_same_tax_count = 2
        amount = dec('10')
        amount_per_receiver = amount / dec('%i' % user_with_same_tax_count)
        self._make_transaction('%s, %s' % (STELLA_TAX_ID, SEAN_TAX_ID), 'Steve', amount)
        users = {x: uget(username=x) for x in usernames}

        self.assertEqual(users['Steve'].balance + amount, balances['Steve'])
        self.assertEqual(users['Stella'].balance - amount_per_receiver, balances['Stella'])
        self.assertEqual(users['Sean'].balance - amount_per_receiver, balances['Sean'])

    def tearDown(self):
        User.objects.all().delete()
        TransactionReceiver.objects.all().delete()
        Transaction.objects.all().delete()
