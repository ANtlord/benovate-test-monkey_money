from django.contrib.auth import get_user_model

User = get_user_model()

STEVE_TAX_ID = '1234567890'
STELLA_TAX_ID = '1111111111'
SEAN_TAX_ID = '2222222222'


def _setup_users():
    User.objects.create(username='Steve', balance=100, tax_id=STEVE_TAX_ID)
    User.objects.create(username='Stevie', balance=0, tax_id=STEVE_TAX_ID)
    # User.objects.create(username='Stevion', balance=0, tax_id=STEVE_TAX_ID)
    User.objects.create(username='Stella', balance=100, tax_id=STELLA_TAX_ID)
    User.objects.create(username='Sean', balance=0, tax_id=SEAN_TAX_ID)
