from decimal import Decimal as dec
from unittest.case import TestCase

# from django.test import TestCase as FTestCase
# from django.utils.unittest import TestCase as UTestCase
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model

from apps.billing.models import Transaction

from .shortcuts import _setup_users
from .shortcuts import User


class CleanTransactionTest(TestCase):
    def setUp(self):
        _setup_users()

    def test_user_balance(self):
        """Testing positive balance of user."""
        sean = User.objects.get(username='Sean')
        stella = User.objects.get(username='Stella')
        with self.assertRaises(ValidationError) as error:
            Transaction(from_user_id=sean.id, amount=dec('0.01')).full_clean()

        Transaction(from_user_id=stella.id, amount=dec('0.01')).full_clean()

    def test_amount(self):
        """Testing transaction amount."""
        stella = User.objects.get(username='Stella')
        with self.assertRaises(ValidationError) as error:
            Transaction(from_user_id=stella.id, amount=dec('0.00')).full_clean()

        with self.assertRaises(ValidationError) as error:
            Transaction(from_user_id=stella.id, amount=dec('-0.01')).full_clean()

        Transaction(from_user_id=stella.id, amount=dec('0.01')).full_clean()

    def tearDown(self):
        User.objects.all().delete()
