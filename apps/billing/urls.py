from django.conf.urls import url
from .views import CreateTransactionView, JustView

urlpatterns = [
    url(r'^$', CreateTransactionView.as_view(), name='create_transaction'),
    url(r'^qwe/$', JustView.as_view(), name='just_viek')
]
