from __future__ import absolute_import, unicode_literals
from celery import shared_task, task
import time

# @shared_task
@task
def add(a, b):
    return a + b


@task
def waitfor(seconds):
    time.sleep(seconds)
    return 1
