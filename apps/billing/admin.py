from django.contrib import admin
from .models import Transaction
from .models import TransactionReceiver

class TransactionReceiverInline(admin.TabularInline):
    model = TransactionReceiver


class TransactionAdmin(admin.ModelAdmin):
    list_display = 'from_user', 'amount', 'admin_receivers'
    inlines = [TransactionReceiverInline]

# Register your models here.
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(TransactionReceiver)
