from django.shortcuts import render
from django.views.generic import CreateView, View
from .forms import TransactionForm
from django.core.urlresolvers import reverse_lazy
from django.http import JsonResponse
from apps.billing.tasks import add, waitfor
from django.http.response import HttpResponse

class JustView(View):
    def get(self, request, *args, **kwargs):
        res = waitfor.delay(20)
        res2 = waitfor.delay(20)
        return HttpResponse("%s<br/>%s" % (res, res2))


class CreateTransactionView(CreateView):
    template_name = 'create_transaction.html'
    form_class = TransactionForm
    success_url = reverse_lazy('create_transaction')

    def get_context_data(self, **kwargs):
        ctx = super(CreateTransactionView, self).get_context_data(**kwargs)
        try:
            ctx['form'].fields['from_user'].widget.attrs['class'] = 'form-control'
        except Exception as e:
            pass
        return ctx

    def form_valid(self, form):
        res = super(CreateTransactionView, self).form_valid(form)
        if self.request.is_ajax():
            return JsonResponse({'status': 0})
        return res

    def form_invalid(self, form):
        res = super(CreateTransactionView, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse({
                'status': 1,
                'error_fields': [x for x in form.errors.keys()],
                'error_html': form.errors.as_ul()
            })
        return res
