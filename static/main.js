(function () {
    function highlightErrorFields(errors, $fields) {
        $fields.each(function (index) {
            var $field = $($fields[index]);
            if (errors.indexOf($field.attr('name')) > -1) {
                $field.addClass('error-input');
            } else {
                $field.removeClass('error-input');
            }
        });
    }

    $('.transaction-form').submit(function (e) {
        e.preventDefault();
        var $this = $(this);
        var postData = $this.serialize()
        $('.success-message').addClass('g-hidden');
        $.post(CREATE_TRANSACTION_URL, postData,
            function (data, textStatus, jqXHR) {
                var $fields = $this.find('input');
                if (data['status']) {
                    var errors = data['error_fields'];
                    highlightErrorFields(errors, $fields);
                    $('.errors').html(data['error_html']);
                } else {
                    $fields.removeClass('error-input');
                    $this[0].reset();
                    $('.errors').html('');
                    $('.success-message').removeClass('g-hidden');
                }
            }
        );
        return false;
    });
})();
