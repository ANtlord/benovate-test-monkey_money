FROM python:3.6.1
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD reqs.pip /code/
RUN pip install -r reqs.pip
ADD . /code/
EXPOSE 8000
ENTRYPOINT ["./manage.py", "runserver"]
