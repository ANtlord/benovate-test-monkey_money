# Test project for benovate

## Installation

* `git clone https://ANtlord@bitbucket.org/ANtlord/benovate-test.git`
* `pip install -r reqs.pip` from project folder
* rename `monkey_money/local_settings.py.dist` to `monkey_money/local_settings.py`
* setup your setting in this file.
* setup Postgres database
* `./manage.py runserver` from project folder
